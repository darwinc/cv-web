# Hoja de vida en formato Web

Para modificar el html estatico se hacen los cambios:
- En el archivo cv_web.Rmd modificar los datos 
- Guardar los cambios realizados
- Ejecutar en consola el siguiente comando 

```
rmarkdown::render('cv_web.Rmd', 'pagedown::html_resume', output_file='public/index.html',encoding="UTF-8")

````